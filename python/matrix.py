import os
from dotenv import load_dotenv
from pathlib import Path
from matrix_controller import MatrixController
from app.events import *
import json

env_path = Path('.') / '.flaskenv'
load_dotenv(dotenv_path=env_path)

def main():
    controller = MatrixController()
    path = os.environ.get('FIFO_PATH')

    try:
        os.mkfifo(path)
    except FileExistsError:
        # the file already exists
        print("%s already exists while starting", path)
    except:
        print("Error")
    controller.process(path)

if __name__ == "__main__":
  main()