from datetime import datetime, timedelta
from screensaver import F1NextRaceScreensaver

# Tests involving the RGB Matrix Library are not tested in the CI/CD process right now due to hardware and architecture constraints

#Checking hours until race string
def testNineHoursUntilRace():
        now = datetime.today()
        nineHoursFuture = datetime.now() + timedelta(hours=9) + timedelta(minutes=5)
        assert F1NextRaceScreensaver.getTimeUntilRaceString(nineHoursFuture).startswith('00:09:04:5') #Not measuring exact time difference because the getTime...() method references 'now'

def testNoneTimeUntilRace():
        assert F1NextRaceScreensaver.getTimeUntilRaceString(None) == '00:00:00:00'

def testRaceInPast():
        now = datetime.today()
        nineHoursFuture = datetime.now() + timedelta(hours=-9)
        assert F1NextRaceScreensaver.getTimeUntilRaceString(nineHoursFuture) == '00:00:00:00'

#Days until race
def testDaysUntilRace():
        dt = datetime.today()
        dt + timedelta(days=3)
        dateStr = dt.strftime("%Y-%m%d")
        timeStr = dt.strftime("%H:%M:%SZ")
        sampleRaceWithTime = {'date':dateStr, 'time':timeStr}
        assert F1NextRaceScreensaver.getDaysUntil(dt) == 3

def testDaysUntilRace():
        assert F1NextRaceScreensaver.getDaysUntil(None) == -1