import sys
import os
from datetime import datetime
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/rpi_rgb_led_matrix/bindings/python')) #keep this path pointing to the python binding of the matrix library
from rgbmatrix import *
from rgbmatrix import graphics
from f1 import F1API
from PIL import Image

WHITE_TEXT_COLOR = graphics.Color(255, 255, 255)
RED_COLOR = graphics.Color(255, 0, 0)
medBoldFont = graphics.Font()
medBoldFont.LoadFont("python/rpi_rgb_led_matrix/fonts/6x13B.bdf")
smallFont = graphics.Font()
smallFont.LoadFont("python/rpi_rgb_led_matrix/fonts/4x6.bdf")
medFont = graphics.Font()
medFont.LoadFont("python/rpi_rgb_led_matrix/fonts/5x8.bdf")

class Screensaver():

    def __init__(self, matrix: RGBMatrix):
        self.matrix = matrix
        self.offscreen_canvas = self.matrix.CreateFrameCanvas()
    
    def iterate(self):
        self.offscreen_canvas.Clear()
        self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

    def wrapText(canvas, font, fontColor, x, height, text):
        length = graphics.DrawText(canvas, font, x, height, fontColor, text)
        x -= 1
        if (x + length < 0):
            x = canvas.width
        return x

    def getType(self):
        return 'DO NOT USE'

    @staticmethod
    def getInstance(type: str, matrix: RGBMatrix) :
        if (type == 'Formula 1') :
            return F1Screensaver(matrix)
        else:
            return BlankScreensaver(matrix)

class BlankScreensaver(Screensaver):
    def __init__(self, matrix: RGBMatrix):
        super().__init__(matrix)

    def iterate(self):
        self.offscreen_canvas.Clear()
        self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

    def getType(self):
        return 'Blank'

class F1Screensaver(Screensaver):
    def __init__(self, matrix: RGBMatrix):
        super().__init__(matrix)
        self.currentScreensaverInd = 0
        self.dt = datetime.today()
        self.f1api = F1API()
        self.nextRaceSSaver = F1NextRaceScreensaver(matrix, self.f1api)
        self.ScreensaverList = [self.nextRaceSSaver, F1DriverChampScreensaver(matrix, self.f1api), F1ConstructorChampScreensaver(matrix, self.f1api)]
        #self.ScreensaverList = [F1ConstructorChampScreensaver(matrix, self.f1api)]

    def iterate(self):  
        diff = datetime.today() - self.dt
        if self.nextRaceSSaver.shouldDisplay():
            self.nextRaceSSaver.iterate()
        else:
            if diff.seconds > 60:
                self.dt = datetime.today()
                self.currentScreensaverInd = (self.currentScreensaverInd+1)%len(self.ScreensaverList)
            self.ScreensaverList[self.currentScreensaverInd].iterate()
    def getType(self):
        return 'Formula 1'

class F1ConstructorChampScreensaver(Screensaver):
    def __init__(self, matrix: RGBMatrix, api: F1API):
        super().__init__(matrix)
        self.f1api = api
        print('Initialize F1 Constructor Championship Screensaver')
        self.pos = self.offscreen_canvas.height + 8
        self.f1api = api
        self.standingsList = self.f1api.getConstructorStandings(True)

    def getConstructorChampionshipRow(constructorObj):
        constructor = constructorObj.get('Constructor')
        code = ''
        points = constructorObj.get('points', '').rjust(3, ' ')
        if constructor is not None:
            code = constructor.get('name','')
            #TODO: Modify this to cut off last all but first 2 words of team name (i.e. Haas F1 Team & Red Bull)
        text = points + ' ' + code
        return text

    def iterate(self):
        self.offscreen_canvas.Clear()
        if self.standingsList is not None:
            season = self.standingsList.get('season', '')
            seasonRound = self.standingsList.get('round', '')
            standings = self.standingsList.get('ConstructorStandings', [])

            headerText = season + ': Rnd ' + seasonRound
            graphics.DrawText(self.offscreen_canvas, medFont, 2, self.pos, WHITE_TEXT_COLOR, headerText)
            graphics.DrawLine(self.offscreen_canvas, 1, self.pos+2, self.offscreen_canvas.width-1, self.pos+2, RED_COLOR) #Table header
            graphics.DrawLine(self.offscreen_canvas, 15, self.pos+2, 15, self.pos+(len(standings)*9), RED_COLOR) #Points separator
            for idx, driver in enumerate(standings):
                text = F1ConstructorChampScreensaver.getConstructorChampionshipRow(driver)
                graphics.DrawText(self.offscreen_canvas, smallFont, 2, self.pos + ((idx+1) * 9), WHITE_TEXT_COLOR, text)

            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)
            self.pos -= 1

            if (self.pos < -(10 + len(standings)*9)):
                self.pos = self.offscreen_canvas.height

        else:
            text = "No driver standings."
            self.pos = Screensaver.wrapText(self.offscreen_canvas, medBoldFont, WHITE_TEXT_COLOR, self.pos, 20, text)
            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

            #Can't just re-initialize because we would re-instantiate the F1API & would blast Ergast with requests. 
            #Our API Handler will attempt to pull a new race every hour.
            self.driverStandings = self.f1api.getDriverStandings(False)

class F1DriverChampScreensaver(Screensaver):
    def __init__(self, matrix: RGBMatrix, api: F1API):
        super().__init__(matrix)
        self.f1api = api
        print('Initialize F1 Driver Championship Screensaver')
        self.pos = self.offscreen_canvas.height + 8
        self.f1api = api
        self.standingsList = self.f1api.getDriverStandings(True)

    def getDriverChampionshipRow(driverObj):
        if driverObj is None:
            driverObj = {}
        position = driverObj.get('position', '').rjust(2, ' ')
        points = driverObj.get('points', '')
        driver = driverObj.get('Driver', {})
        code = driver.get('code','').rjust(3, ' ')
        number = driver.get('permanentNumber', '').rjust(2, ' ')
        text = position + ' ' + code + ' ' + number + ' ' + points
        return text

    def iterate(self):
        self.offscreen_canvas.Clear()
        if self.standingsList is not None:
            season = self.standingsList.get('season', '')
            seasonRound = self.standingsList.get('round', '')
            standings = self.standingsList.get('DriverStandings', [])

            headerText = season + ': Rnd ' + seasonRound
            graphics.DrawText(self.offscreen_canvas, medFont, 2, self.pos, WHITE_TEXT_COLOR, headerText)
            graphics.DrawLine(self.offscreen_canvas, 1, self.pos+2, self.offscreen_canvas.width-1, self.pos+2, RED_COLOR) #Table header
            graphics.DrawLine(self.offscreen_canvas, 41, self.pos+2, 41, self.pos+(len(standings)*9), RED_COLOR) #Points separator
            for idx, driver in enumerate(standings):
                text = F1DriverChampScreensaver.getDriverChampionshipRow(driver)
                graphics.DrawText(self.offscreen_canvas, smallFont, 4, self.pos + ((idx+1) * 9), WHITE_TEXT_COLOR, text)

            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)
            self.pos -= 1

            if (self.pos < -(10 + len(standings)*9)):
                self.pos = self.offscreen_canvas.height

        else:
            text = "No driver standings."
            self.pos = Screensaver.wrapText(self.offscreen_canvas, medBoldFont, WHITE_TEXT_COLOR, self.pos, 20, text)
            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

            #Can't just re-initialize because we would re-instantiate the F1API & would blast Ergast with requests. 
            #Our API Handler will attempt to pull a new race every hour.
            self.driverStandings = self.f1api.getDriverStandings(False)



class F1NextRaceScreensaver(Screensaver):
    def __init__(self, matrix: RGBMatrix, api: F1API):
        super().__init__(matrix)
        self.f1api = api
        self.pos = self.offscreen_canvas.width
        self.f1api = api
        self.nextRace = self.f1api.getNextRace(True)
        self.image = None
        imageFound = F1API.getGetCircuitImageFromRace(self.nextRace)
        if imageFound is not None:
            self.image = imageFound

    def getTimeUntilRaceString(dt):
        timeUntil = '00:00:00:00'
        if (dt is not None and dt > datetime.today()):
            diff = dt - datetime.today()
            daysUntil = 0
            hoursUntil = 0
            minutesUntil = 0
            secondsUntil = 0
            daysUntil = diff.days
            hoursUntil = (diff.seconds % 86400) // 3600
            minutesUntil = (diff.seconds % 3600) // 60
            secondsUntil = diff.seconds % 60
            timeUntil = str(daysUntil).rjust(2,'0') + ':' + str(hoursUntil).rjust(2,'0') + ':' + str(minutesUntil).rjust(2,'0') + ':' + str(secondsUntil).rjust(2,'0')
        return timeUntil
        
    def getDaysUntil(dt):
        daysUntil = -1
        if (dt is not None and dt > datetime.today()):
            diff = dt - datetime.today()
            daysUntil = diff.days
        return daysUntil

    def iterate(self):
        self.offscreen_canvas.Clear()
        if self.nextRace is not None:
            raceName = self.nextRace.get('raceName', '')
            circuitName = F1API.getCircuitNameFromRace(self.nextRace)
            nameDisplay = raceName + ' - ' + circuitName if circuitName is not None and circuitName != '' else raceName
            dt = F1API.getDateTimeFromRace(self.nextRace)
            timeUntil = F1NextRaceScreensaver.getTimeUntilRaceString(dt)

            if self.image is not None:
                self.offscreen_canvas.SetImage(self.image, 0, 0)

            #Text (race name & time until)
            self.pos = Screensaver.wrapText(self.offscreen_canvas, medBoldFont, WHITE_TEXT_COLOR, self.pos, 10, nameDisplay)
            graphics.DrawText(self.offscreen_canvas, medFont, 5, 20, WHITE_TEXT_COLOR, timeUntil)

            #Lights Out!
            daysUntil = F1NextRaceScreensaver.getDaysUntil(dt)
            for i in range(0, 5):
                if (daysUntil <= i and daysUntil >= 0):
                    circleXPos = self.offscreen_canvas.width - (7+(i*12))
                    circleYPos = 27
                    for rad in range(1,6):
                        graphics.DrawCircle(self.offscreen_canvas, circleXPos, circleYPos, rad, RED_COLOR)
            
            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

            #If the race start time has passed, check to see if there's a new one available. If not, clear the race & wait for reinitialization
            if dt < datetime.today():
                #Can't just re-initialize because we would re-instantiate the F1API & would blast Ergast with requests. 
                self.nextRace = self.f1api.getNextRace(False)
                self.image = F1API.getGetCircuitImageFromRace(self.nextRace)

        else:
            text = "No upcoming races."
            self.pos = Screensaver.wrapText(self.offscreen_canvas, medBoldFont, WHITE_TEXT_COLOR, self.pos, 20, text)
            self.offscreen_canvas = self.matrix.SwapOnVSync(self.offscreen_canvas)

            #Can't just re-initialize because we would re-instantiate the F1API & would blast Ergast with requests. 
            #Our API Handler will attempt to pull a new race every hour.
            self.nextRace = self.f1api.getNextRace(False)
            imageFound = self.f1api.getGetCircuitImageFromRace(self.nextRace)
            self.image = imageFound

    #Checks to see if we should prioritize this screensaver because the race is about to start
    def shouldDisplay(self):
        dt = F1API.getDateTimeFromRace(self.nextRace)
        diff = dt - datetime.today()
        return dt > datetime.today() and diff.days < 2

    def getType(self):
        return 'Formula 1'