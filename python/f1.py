import requests
from datetime import datetime, timedelta
import os
from PIL import Image


requestDict = {}

class F1API():

    #def __init__(self):
        #Do nothing

    def getNextRace(self, forceRemoteGet: bool):
        nextRaces = self.getErgastData(forceRemoteGet, "http://ergast.com/api/f1/current/next.json")['RaceTable']
        if len(nextRaces['Races']) > 0:
            return nextRaces['Races'][0]
        else:
            return None

    def getDriverStandings(self, forceRemoteGet: bool):
        return self.getErgastData(forceRemoteGet, "http://ergast.com/api/f1/current/driverStandings.json").get('StandingsTable', {}).get('StandingsLists', [])[0]

    def getConstructorStandings(self, forceRemoteGet: bool):
        return self.getErgastData(forceRemoteGet, "http://ergast.com/api/f1/current/constructorStandings.json").get('StandingsTable', {}).get('StandingsLists', [])[0]

    def getErgastData(self, forceRemoteGet: bool, url: str):
        response = None
        if not forceRemoteGet and url in requestDict:
            cachedResponse = requestDict[url]
            dt = cachedResponse.get('Date')
            hourAgo = datetime.today() + timedelta(hours = -1)
            if dt is None or dt < hourAgo:
                response = requests.get(url, False).json()
                response = response.get('MRData', {})
                requestDict[url] = {"Date": datetime.today(), "Response": response}
                print("Refreshing cached value from Ergast")
            else:
                response = cachedResponse['Response']
                print("Returning cached response")
        else:
            print("Forcing remote Ergast request from " + url)
            response = requests.get(url, False).json()
            response = response.get('MRData', {})
            requestDict[url] = {"Date": datetime.today(), "Response": response}
        
        return response

    def getGetCircuitImageFromRace(race):
        image = None
        if (race is not None):
            path = os.path.dirname(__file__) + '/resources/f1-tracks/'+race.get('Circuit', {}).get('circuitId', 'doesntexist')+'.jpg'
            file_exists = os.path.exists(path)
            if (file_exists):
                image = Image.open(path).convert('RGB')
            else:
                print("No file exists at: " + path)
        else:
            print("Race was None. Could not get image.")
        return image


    def getDateTimeFromRace(race):
        dt = None
        if race is not None:
            raceDateTime = race.get('date', '') + ' ' + race.get('time', '') #Race date and time is always passed GMT
            dt = datetime.strptime(raceDateTime, '%Y-%m-%d %H:%M:%SZ')
        return dt

    def getCircuitNameFromRace(race):
        if (race is not None and 'Circuit' in race and 'circuitName' in race['Circuit']):
            return race['Circuit']['circuitName']
        else:
            print("No circuit record available to determine name from: ")
            print(race)
            return ''