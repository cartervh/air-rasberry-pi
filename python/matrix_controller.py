import argparse
import time
import sys
import os
import math
from datetime import datetime
from datetime import date
from app.events import *
from screensaver import Screensaver
from app.screensaver_controller import ScreensaverController

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/rpi_rgb_led_matrix/bindings/python')) #keep this path pointing to the python binding of the matrix library
from rgbmatrix import *
from rgbmatrix import graphics

blueRGB = [0,123,255]
redRGB = [220,40,0]
greenRGB = [40,200,0]



class MatrixController():
    def __init__(self):
        self.lastEvent = None
        self.screensaver = None
        self.options = RGBMatrixOptions()

        #These are all environment variables. Set either in the .flaskenv file, or in the Dockerfile
        self.options.hardware_mapping = os.environ.get('LED_GPIO_MAPPING')
        self.options.rows = int(os.environ.get('LED_ROWS'))
        self.options.cols = int(os.environ.get('LED_COLS'))
        self.options.chain_length = int(os.environ.get('LED_CHAIN'))
        self.options.parallel = int(os.environ.get('LED_PARALLEL'))
        self.options.row_address_type = int(os.environ.get('LED_ROW_ADDR_TYPE'))
        self.options.multiplexing = int(os.environ.get('LED_MULTIPLEXING'))
        self.options.pwm_bits = int(os.environ.get('LED_PWM_BITS'))
        self.options.brightness = int(os.environ.get('LED_BRIGHTNESS'))
        self.options.pwm_lsb_nanoseconds = int(os.environ.get('LED_PWM_LSB_NANOSECONDS'))
        self.options.led_rgb_sequence = os.environ.get('LED_RGB_SEQUENCE')
        self.options.pixel_mapper_config = os.environ.get('LED_PIXEL_MAPPER')
        self.options.show_refresh_rate = int(os.environ.get('LED_SHOW_REFRESH'))
        self.options.gpio_slowdown = int(os.environ.get('GPIO_SLOWDOWN'))
        self.options.disable_hardware_pulsing = os.environ.get('DISABLE_HARDWARE_PULSING')
        
        self.matrix = RGBMatrix(options = self.options)


    def process(self, path):
        try:
            while True:
                try:
                    fifo = os.open(path, os.O_RDONLY | os.O_NONBLOCK) #Opening with r/w because otherwise this blocks
                    text = os.read(fifo, 800) #Instead of doing this the smart way and only parsing complete JSON, we're just increasing our read size to one where we expect that we won't be overwhelmed with events
                    text = text.decode("utf-8")
                    text = text.strip()
                    os.close(fifo)
                    splits = text.split("\n")
                    for split in splits:
                        if (len(split) > 0):
                            self.run(EventFactory().parseEvent(split))
                    if self.lastEvent is None or isinstance(self.lastEvent, ShutDownEvent):
                        screensaverType = ScreensaverController().getScreensaver()
                        if self.screensaver is None or self.screensaver.getType() != screensaverType:
                            self.screensaver = Screensaver.getInstance(screensaverType, self.matrix)
                        self.screensaver.iterate()
                    else:
                        self.screensaver = None
                        
                except OSError as e:
                    if e.errno != 11:
                        raise e
                time.sleep(0.1)
        except KeyboardInterrupt:
            sys.exit(0)

        return True

    def run(self, event):
        self.lastEvent = event
        if isinstance(event, OnCallEvent):
            self.displayCallStatus(event)
        elif isinstance(event, ShutDownEvent):
            self.shutdownMatrix()
        else:
            self.shutdownMatrix()

    def setPixelWithArray(self, x, y, ar):
        self.matrix.SetPixel(x, y, ar[0], ar[1], ar[2])
    
    def shutdownMatrix(self):
        width = self.matrix.width
        height = self.matrix.height
        self.changeRangeToColor(0, 0, self.matrix.width, self.matrix.height, [0,0,0])

    def displayCallStatus(self, event):
        leftRGB = [0,0,0]
        rightRGB = [0,0,0]
        rgbAr = []
        for stat in event.status:
            if event.status[stat]['status'] == 'On':
                rgbAr.append(redRGB)
            elif event.status[stat]['status'] == 'Focus':
                rgbAr.append(blueRGB)
            else:
                rgbAr.append(greenRGB)
        
        shiftLength = math.floor(self.matrix.width/len(event.status))
        for ind in range(0, len(event.status)):
            startY = 0
            endY = self.matrix.height
            startX = ind * shiftLength
            endX = (ind+1) * shiftLength
            self.changeRangeToColor(startX, startY, endX, endY, rgbAr[ind])
    
    def changeRangeToColor(self, startX, startY, endX, endY, arNewColor):
        for x in range(endX, startX, -1):
            i = 0
            while x + i < endX:
                self.setPixelWithArray(x + i, startY + i, arNewColor)
                i+=1
            time.sleep(.01)
        for y in range(startY, endY):
            i = 0
            while y + i < endY:
                self.setPixelWithArray(startX + i, y + i, arNewColor)
                i+=1
            time.sleep(.01)
