from enum import Enum
import json
import traceback

class EventType(Enum):
    ON_CALL = "On Call"
    SHUT_DOWN = "Shut Down"

class EventFactory():
    def parseEvent(self, line):
        try:
            print(line)
            data = json.loads(line)
            retEvent = ShutDownEvent()
            if data["type"] == EventType.ON_CALL.value:
                retEvent = OnCallEvent(data["data"])
            elif data["type"] == EventType.SHUT_DOWN.value:
                retEvent = ShutDownEvent()
            return retEvent
        except:
            traceback.print_exc()
            return ShutDownEvent()
    
    def parseJSON(self, data):
        retEvent = None
        if data["type"] == EventType.ON_CALL.value:
            retEvent = OnCallEvent(data["data"])
        elif data["type"] == EventType.SHUT_DOWN.value:
            retEvent = ShutDownEvent()
        return retEvent


class MatrixEvent():
    def __init__(self, eventType):
        self.type = eventType
    
    def convertToJSON(self):
        pass

class OnCallEvent(MatrixEvent):
    def __init__(self, onCallStatus):
        super().__init__(EventType.ON_CALL)
        self.status = onCallStatus

    def convertToJSON(self):
        json = {
            'type': self.type.value,
            'data': self.status
        }
        return json

class ShutDownEvent(MatrixEvent): 
    def __init__(self):
        super().__init__(EventType.SHUT_DOWN)
    
    def convertToJSON(self):
        json = {
            'type': self.type.value
        }
        return json