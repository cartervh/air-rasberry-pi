//Add status without using a timer
function updateStatus(onAirCard, expires) {
  person = onAirCard.attr('id'),
  newStatus = onAirCard.data('btnType')
  var updateData = {'person':person, 'onCall':newStatus, 'expires':expires}
  $.post('/update-status', updateData, function( data ) {
      updateUI(data);
  });
}

//Get the current statuses from the server
function getStatus() {
  $.get('/update-status', function( data ) {
      updateUI(data);
    });
}


function updateUI(data) {
  for (i in data) {
    var status = data[i]['status']
    var expires = data[i]['expires']
    var fromNow = ''
    if (expires != '') {
      var expireDate = moment(expires)
      var today = moment()
      if (expireDate.isAfter(today)) {
        fromNow = 'Done ' + moment(expires).fromNow()
      }
    }
    var $card = $('#' + i);
    var $img = $($card.find('.card-img-top'))
    var $text = $($card.find('.card-title'))
    var $timeRemaining = $($card.find('.time-remaining-overlay'))
    $timeRemaining.html(fromNow)
    if (status == 'On') {
      $img.attr('src', '/static/red.png');
      $text.html(i + ' is on a call');
    } else if (status == 'Focus') {
      $img.attr('src', '/static/blue.png');
      $text.html(i + ' is focusing');
    } else {
      $img.attr('src', '/static/green.png');
      $text.html(i + ' is not on a call');
    }
  }
}

function shutDown() {
  $.post('/shut-down', function( data ) {
    updateUI(data);
  });
}

function doPoll(){
  $.get('/update-status', function( data ) {
      updateUI(data);
      setTimeout(doPoll,15000);
  });
}

//Run from the .on-air-card div
function openTimerDiv(onAirCard, btnType) {
  onAirCard.find('.timer-div').slideDown();
  onAirCard.find('.text-muted').html(btnType + '. For how long?')
  onAirCard.data('btnType', btnType);
}

//Run from the .on-air-card div
function closeTimerDiv(onAirCard) {
  onAirCard.find('.timer-div').slideUp(function() {
    onAirCard.find('.status-time-input').val('');
  });

}

function setPrefs() {
  localStorage.setItem('userName', $("#UserSelect").val())
  setScreensaver();
  postUpdatePrefs()
}

function getScreensaver() {
  $.get('/screensaver', function( resp ) {
    updateScreensaver(resp);
  });
}

function setScreensaver(newScreensaver) {
  var updateData = {'screensaver':$("#ScreensaverSelect").val()}
  $.post('/screensaver', updateData, function(resp) {
    updateScreensaver(resp);
  });
}

function updateScreensaver(resp) {
  $("#ScreensaverSelect").val(resp)
}

function postUpdatePrefs() {
  if (!localStorage.getItem('userName')) {
    $("#Messages").html("No user selected!")
  } else {
    $("#Messages").html("")
  }
  $("#UserSelect").val(localStorage.getItem('userName'));

  getScreensaver();
}

function toggleUserOnCall() {
  const user = localStorage.getItem('userName');
  console.log("#" + user);
  onAirCard = $("#" + user)
  var btnType = onAirCard.data('btnType');
  console.log(btnType)
  if (btnType == 'Off') {
    btnType = 'On'
  } else {
    btnType = 'Off'
  }
  onAirCard.data('btnType', btnType);
  updateStatus(onAirCard, '');
}

$(document).ready(function() {
  $('.on-air-card [name="off-call-btn"]').click(function(event) {
    var onAirCard = $(this).closest('.on-air-card')
    var btnType = $(this).data('btnType')
    onAirCard.data('btnType', btnType);
    closeTimerDiv(onAirCard)
    updateStatus($(this).closest('.on-air-card'), '')
  })

  document.body.onkeyup = function(e){
    if (e.code === 'Space') {
      e.preventDefault()
      toggleUserOnCall()
    }
}

postUpdatePrefs();

  //Single clicks for timeable events open the timer div
  $('.on-air-card .timeable').click(function(e) {
      var that = this;
      var btnType = $(this).data('btnType');
      var onAirCard = $(that).closest('.on-air-card')
      setTimeout(function() {
          var dblclick = parseInt($(that).data('double'), 10);
          if (dblclick > 0) {
              $(that).data('double', dblclick-1);
          } else {
              openTimerDiv(onAirCard, btnType);
          }
      }, 300);
  })
  
  //Always send event when status button double clicked
  $('.on-air-card .status-btn').dblclick(function(e) {
      $(this).data('double', 2);
      var onAirCard = $(this).closest('.on-air-card')
      var btnType = $(this).data('btnType');
      onAirCard.data('btnType', btnType);
      closeTimerDiv(onAirCard);
      updateStatus(onAirCard, '')
  });

  //Send timer event
  $('.submit-timer-btn').click(function (e) {
    var mins = $(this).closest('.input-group').find('.status-time-input').val()
    var onAirCard = $(this).closest('.on-air-card')
    if (mins > 0) {
      var statusEndTime = moment().add(mins, 'm');
      updateStatus(onAirCard, statusEndTime.toJSON())
    } else if (mins < 0) {
      alert("This is not a valid time!")
    } else {
      updateStatus(onAirCard, '')
    }

    closeTimerDiv(onAirCard)
  })



  $('.shut-down').click(function(event) {
    shutDown()
  })

  $('#preferences').on('shown.bs.modal', function () {
    getScreensaver()
  })

  doPoll();
});