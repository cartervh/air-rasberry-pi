import os
import sys
from flask import Flask, render_template, request, jsonify
import json
from datetime import datetime
from events import *
#sys.path.append("..") 
from screensaver_controller import ScreensaverController

onCallStatus = {
    'Carter': {'status':'Off', 'expires':''},
    'Christina':{'status':'Off', 'expires':''}
}

screensavers = ['Formula 1', 'Blank']

path = os.environ.get('FIFO_PATH')

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', len=len(onCallStatus), onCallDict=onCallStatus, screensavers=screensavers)

# Resource used to update a person's status
@app.route("/update-status", methods=['GET', 'POST'])
def updateStatus():
    if (request.method == 'POST'):
        #Update status
        foundPerson = request.form['person']
        if request.form['person'] in onCallStatus:
            onCallStatus[foundPerson] = {'status':request.form['onCall'], 'expires':request.form['expires']}
            
        else:
            print('No person found:', foundPerson)
        
        writeStatusToFifo()
    else:
        #Check if we need to expire something
        for key in onCallStatus:
            foundPerson = onCallStatus.get(key)
            if foundPerson.get('expires') != '':
                timeStr = foundPerson.get('expires')
                timeStr = timeStr[0:len(timeStr)-5]
                dt = datetime.strptime(timeStr, '%Y-%m-%dT%H:%M:%S')
                now = datetime.utcnow()
                if now > dt:
                   onCallStatus[key] = {'status':'Off', 'expires':''} 
                   writeStatusToFifo()
    return jsonify(onCallStatus) 

#Updates or gets the screensaver
@app.route("/screensaver", methods=['POST', 'GET'])
def screensaver():
    if (request.method == 'POST'):
        ScreensaverController().setScreensaver(request.form['screensaver'])
    screensaver = ScreensaverController().getScreensaver()
    if screensaver is None:
        screensaver = ''
    return screensaver

# Shuts down the matrix (not the web-app)
@app.route("/shut-down", methods=['POST'])
def shutDownMatrix():
    if (request.method == 'POST'):
        event = ShutDownEvent()
        jsonEvent = event.convertToJSON()
        writeToFifo(jsonEvent) 
        for key in onCallStatus:
            onCallStatus[key] = {'status':'Off', 'expires':''}
    return jsonify(onCallStatus)

def writeStatusToFifo():
    event = OnCallEvent(onCallStatus)
    jsonEvent = event.convertToJSON()
    print(jsonEvent)
    writeToFifo(jsonEvent)

def writeToFifo(event):
    print(path)
    fifo = open(path, "w") #If the matrix operator is not running, this will block the thread
    fifo.write(json.dumps(event) + "\n")
    fifo.close()

app.run(host='0.0.0.0', port=8000) 