import sqlite3

class ScreensaverController():
    def __init__(self):
        self.dbConnectionString = '/etc/on-air-db/on-air.db'
        #self.dbConnectionString = ':memory:'
        con = sqlite3.connect(self.dbConnectionString)
        createTableSQL = '''CREATE TABLE IF NOT EXISTS screensaver (screensavertype VARCHAR(255));'''
        cur = con.cursor()
        cur.execute(createTableSQL)
        con.commit()
        con.close()

    def getScreensaver(self):
        con = sqlite3.connect(self.dbConnectionString)
        getScreensaverSQL = '''SELECT screensavertype FROM screensaver;'''
        cur = con.cursor()
        sType = None
        cur.execute(getScreensaverSQL)
        sType = cur.fetchone()
        if (sType is not None):
            sType = sType[0]
        con.commit()
        con.close()
        return sType
    
    def setScreensaver(self, screensaverType:str):
        screensaverSQL = None
        if self.getScreensaver() is not None:
            screensaverSQL = '''UPDATE screensaver SET screensavertype = ?'''
            print('Updating')
        else:
            screensaverSQL = '''INSERT INTO screensaver(screensavertype) values (?)'''
            print('Inserting')
        
        con = sqlite3.connect(self.dbConnectionString)
        cur = con.cursor()
        cur.execute(screensaverSQL, (screensaverType,))
        con.commit()
        con.close()

