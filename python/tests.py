from datetime import datetime

from f1 import F1API

def testRaceDateTime():
        sampleRaceWithTime = {'date':'2022-01-03', 'time':'05:06:37Z'}
        correctDateTime = datetime(2022,1,3,5,6,37)
        testTime = F1API.getDateTimeFromRace(sampleRaceWithTime)
        assert testTime == correctDateTime

def testEmptyRaceDateTime():
        testTime = F1API.getDateTimeFromRace(None)
        assert testTime is None

#Circuit Images
def testGetImageForMonza():
        sampleMonza = {'Circuit': {'circuitId':'monza'}}
        image = F1API.getGetCircuitImageFromRace(sampleMonza)
        assert image is not None

def testGetBadRaceImage():
        sampleRichmond = {'Circuit': {'circuitId':'richmondIntl'}}
        image = F1API.getGetCircuitImageFromRace(sampleRichmond)
        assert image is None

def testGetNoneRaceImage():
        image = F1API.getGetCircuitImageFromRace(None)
        assert image is None
