#Used for setting flask environment variables
FLASK_APP=app.py
FLASK_ENV=development
FLASK_RUN_HOST=0.0.0.0
FLASK_RUN_PORT=8000

#0-4, use higher for faster pis (I use rpi 4)
GPIO_SLOWDOWN=4

#Number of rows and columns in matrix
LED_ROWS=32 
LED_COLS=64

#Max brightness, 1-100
LED_BRIGHTNESS=50

#Too lazy to look it up
LED_PWM_BITS=9

#Number of parallel chains. Adafruit bonnet only allows 1
LED_PARALLEL=1 

#Number of daisy-chained boards.
LED_CHAIN=1 

#e.g. "Rotate:90"
LED_PIXEL_MAPPER=Rotate:0

#Hardware Mapping: regular, adafruit-hat, adafruit-hat-pwm
LED_GPIO_MAPPING=adafruit-hat

#Sequence of RGB
LED_RGB_SEQUENCE=RGB

LED_ROW_ADDR_TYPE=0
LED_MULTIPLEXING=0
LED_SCAN_MODE=1
LED_PWM_LSB_NANOSECONDS=130
LED_RGB_SEQUENCE=RGB
LED_SHOW_REFRESH=0
LED_NO_HARDWARE_PULSE=True

FIFO_PATH=/tmp/matrix_events.fifo