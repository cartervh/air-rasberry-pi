FROM python:3.7

WORKDIR /app

COPY Pipfile /app
RUN pip install pipenv && \
    apt-get install libjpeg-dev zlib1g-dev && \
    #pipenv install --deploy --system --ignore-pipfile && \
    pipenv lock && \
    pipenv requirements > requirements.txt && \
    pip install -r requirements.txt

COPY . /app

ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=8000

# We copy just the Pipfiles first to leverage Docker cache
#COPY Pipfile* ./
COPY .flaskenv ./
COPY run.sh ./

EXPOSE 8000

USER root

ENTRYPOINT ["bash", "./run.sh"]