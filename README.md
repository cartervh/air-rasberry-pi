# Air Rasberry Pi

This project is designed to be a way to provide several people in the same work environment a seamless (and silent) way to communicate when they are and aren't on a call. 

The interface allows for users to indicate if they are on a call, and also a "shutdown" option to ensure that the LED display can be turned off without killing any of the processes.

## What is it?
The [video you can find here](https://youtu.be/RmCTOqHcREM) shows some of the things that the LED screen can do. It displays some information about Formula 1 and displays the "on-call" status of both users.
The below collection of screenshots shows some of the interface served by Flask. All updates in the UI are reflected in the LED screen.
![Users both free to talk](/images/users-not-on-call.png "Users both free to talk")
![Carter is setting a 30 minute call](/images/user-setting-30-minutes-call.png "Carter is setting a 30 minute call")
![Carter is on a 30 minute call](/images/user-on-30-min-call.png "Carter is on a 30 minute call")
![Christina is focusing indefinitely](/images/user-focusing-indefinitely.png "Christina is focusing indefinitely")
![A user is updating their preferences](/images/user-preferences-screen.png "A user is updating their preferences")




## Requires use of pipenv, python 3.7, Docker, and Docker Compose.
To install necessary packages:
- Clone repository
- Run `pipenv install` from the project's home directory
- If you run into issues with Pillow installing due to lack of dependent libraries regarding JPEGs, you may need to run `sudo apt-get install libjpeg-dev zlib1g-dev`
- Be sure to install both Docker and Docker Compose

## To start the server for local development:
Run `sudo pipenv run python python/app/app.py` from the working directory.

This needs to be done as root for the rgb matrix to work.

In a new terminal, run `sudo pipenv run python python/matrix.py`

Note that any changes to the code handled by flask will get auto-reloaded upon changes, however any matrix related code will not.

## To start the docker container:
Docker build: `docker build . -t on-air:latest`

Debug docker: `docker-compose up`

"Production" docker: `docker-compose up -d`

## To access the web interface:
Determine your pi's IP on your local network, then point your browser to port 80 at that IP. On my local network I have set up a local DNS server using [pi-hole](https://pi-hole.net/) and inserted a DNS record for this project, which makes it nicer to access. Do this at your own risk.

## If using the RGB Matrix
I utilized these parts:
- [Raspberry Pi 4](https://www.adafruit.com/product/4564)
- [64x32 RGB LED Matrix from Adafruit](https://www.adafruit.com/product/2278)
- [Raspberry Pi RGB Matrix Bonnet from Adafruit](https://www.adafruit.com/product/3211)
- [5V 4A switching power supply](https://www.adafruit.com/product/1466)

## Formula 1 API
I have utilized the [Ergast Developer API](http://ergast.com/mrd/) to generate the Formula 1 statistcs used in the F1 screensaver.

A quick tutorial from Adafruit can be found [here](https://learn.adafruit.com/adafruit-rgb-matrix-bonnet-for-raspberry-pi/driving-matrices)
The library used to drive the matrix can be found [here](https://github.com/hzeller/rpi-rgb-led-matrix)

I would recommend using the library documentation as the source of truth, and using the Adafruit guide to get started.
